package org.janahan.foodstandards;

import org.janahan.foodstandards.service.FsaServerException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public abstract class ExeptionHandlingController {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String errorHandler(Exception e) {
        return "Unknown Error: " + e.getMessage();
    }

    @ExceptionHandler(FsaServerException.class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public String fsaServerExceptionHandler(FsaServerException e) {
        return e.getMessage();
    }
}
