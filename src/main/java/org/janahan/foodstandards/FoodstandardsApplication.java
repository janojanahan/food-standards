package org.janahan.foodstandards;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import reactor.netty.tcp.TcpClient;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class FoodstandardsApplication {
    private static final Integer REMOTE_SERVER_TIMEOUT_MILLIS = 10000;

    public static void main(String[] args) {
        SpringApplication.run(FoodstandardsApplication.class, args);
    }

    @Bean
    TcpClient tcpClient() {
        return TcpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, REMOTE_SERVER_TIMEOUT_MILLIS)
                .doOnConnected(connection ->
                        connection.addHandlerLast(new ReadTimeoutHandler(REMOTE_SERVER_TIMEOUT_MILLIS.longValue(), TimeUnit.MILLISECONDS))
                                .addHandlerLast(new WriteTimeoutHandler(REMOTE_SERVER_TIMEOUT_MILLIS.longValue(), TimeUnit.MILLISECONDS)
                                ));
    }
}
