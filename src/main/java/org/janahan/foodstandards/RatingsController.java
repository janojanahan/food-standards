package org.janahan.foodstandards;

import org.janahan.foodstandards.domain.RatingsSummary;
import org.janahan.foodstandards.service.RatingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/api/authority/{id}/ratings", produces = "application/json")
public class RatingsController extends ExeptionHandlingController {


    private final RatingsService ratingsService;

    @Autowired
    public RatingsController(final RatingsService ratingsService) {

        this.ratingsService = ratingsService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Mono<RatingsSummary> getSummary(@PathVariable Long id) {
        return ratingsService.ratingsSummaryForAuthorityAsync(id);
    }
}
