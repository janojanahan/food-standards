package org.janahan.foodstandards.domain;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum HygeneRatingLevel {
    FIVE("5 Stars", "5"),
    FOUR("4 Stars", "4"),
    THREE("3 Stars", "3"),
    TWO("2 Stars", "2"),
    ONE("1 Stars", "1"),
    PASS("Pass", "Pass"),
    NEEDS_IMPROVEMENT("Needs Improvement", "Improvement Required"),
    EXEMPT("Exempt", "Exempt");

    private final String textValue;
    private final String displayValue;

    HygeneRatingLevel(String displayValue, String textValue) {
        this.textValue = textValue;
        this.displayValue = displayValue;
    }

    public static HygeneRatingLevel fromString(String textValue) {
        return Arrays.asList(values()).stream()
                .filter(x -> x.textValue.equals(textValue))
                .findFirst()
                .orElse(EXEMPT);
    }

    @JsonValue
    public String displayValue() {
        return displayValue;
    }
}
