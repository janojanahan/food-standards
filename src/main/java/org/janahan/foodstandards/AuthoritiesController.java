package org.janahan.foodstandards;

import org.janahan.foodstandards.domain.Authority;
import org.janahan.foodstandards.service.AuthoritiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping(path = "/api/authority", produces = "application/json")
public class AuthoritiesController extends ExeptionHandlingController {
    private final AuthoritiesRepository authoritiesRepository;

    @Autowired
    public AuthoritiesController(final AuthoritiesRepository authoritiesRepository) {
        this.authoritiesRepository = authoritiesRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Mono<List<Authority>> getAuthorities() {
        return authoritiesRepository.findAllAuthoritiesAsync();
    }


}
