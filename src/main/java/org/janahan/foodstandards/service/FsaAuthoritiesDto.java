package org.janahan.foodstandards.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
class FsaAuthoritiesDto {

    private final List<FsaAuthorityDto> authorities;

    FsaAuthoritiesDto(@JsonProperty("authorities") List<FsaAuthorityDto> authorities) {
        this.authorities = authorities;
    }

    public List<FsaAuthorityDto> getAuthorities() {
        return authorities;
    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    static class FsaAuthorityDto {
        private final long localAuthorityId;
        private final String name;
        private final String region;

        FsaAuthorityDto(
                @JsonProperty("LocalAuthorityId") long localAuthorityId,
                @JsonProperty("Name") String name,
                @JsonProperty("RegionName") String region) {

            this.localAuthorityId = localAuthorityId;
            this.name = name;
            this.region = region;
        }

        public long getLocalAuthorityId() {
            return localAuthorityId;
        }

        public String getName() {
            return name;
        }

        public String getRegion() {
            return region;
        }
    }
}
