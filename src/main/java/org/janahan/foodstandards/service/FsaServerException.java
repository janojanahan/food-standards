package org.janahan.foodstandards.service;

public class FsaServerException extends RuntimeException {
    public FsaServerException(String message, Throwable cause) {
        super(message, cause);
    }
}
