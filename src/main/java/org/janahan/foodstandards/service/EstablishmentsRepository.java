package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.HygeneRatingLevel;
import reactor.core.publisher.Mono;

import java.util.List;

public interface EstablishmentsRepository {
    Mono<List<HygeneRatingLevel>> findRatingsForAuthority(long authorityId);
}
