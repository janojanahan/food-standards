package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.Authority;
import reactor.core.publisher.Mono;

import java.util.List;

public interface AuthoritiesRepository {
    /**
     * Asynchronously returns a list of all authorities.
     */
    Mono<List<Authority>> findAllAuthoritiesAsync();

    /**
     * Asynchronously find a single Authority by authority by ID.
     */
    Mono<Authority> findAuthorityByIdAsync(long id);
}
