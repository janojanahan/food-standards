package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.Authority;
import org.janahan.foodstandards.domain.HygeneRatingLevel;
import org.janahan.foodstandards.domain.RatingsSummary;
import org.janahan.foodstandards.domain.RatingsSummary.RatingItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.*;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.*;

@Component
public class FsaRatingsService implements RatingsService {
    private static final EnumSet<HygeneRatingLevel> ENGLISH_RATINGS =
            EnumSet.of(FIVE, FOUR, THREE, TWO, ONE, EXEMPT);

    private static final EnumSet<HygeneRatingLevel> SCOTTISH_RATINGS =
            EnumSet.of(PASS, NEEDS_IMPROVEMENT);
    private final AuthoritiesRepository authoritiesRepository;
    private final EstablishmentsRepository establishmentsRepository;

    @Autowired
    public FsaRatingsService(
            final AuthoritiesRepository authoritiesRepository,
            final EstablishmentsRepository establishmentsRepository) {
        this.authoritiesRepository = authoritiesRepository;
        this.establishmentsRepository = establishmentsRepository;
    }

    @Override
    public Mono<RatingsSummary> ratingsSummaryForAuthorityAsync(final long authorityId) {
        final var authority = authoritiesRepository.findAuthorityByIdAsync(authorityId);
        final var listOfRatings =
                establishmentsRepository.findRatingsForAuthority(authorityId);

        return authority.zipWith(listOfRatings, this::createSummary);
    }

    private RatingsSummary createSummary(Authority authority, List<HygeneRatingLevel> hygeneRatings) {
        var ratingsMap = hygeneRatings.stream()
                .collect(groupingBy(Function.identity(),
                        () -> new EnumMap<>(HygeneRatingLevel.class),
                        counting()));

        var ratings = (authority.isScotland() ? SCOTTISH_RATINGS : ENGLISH_RATINGS).stream()
                .map(a -> new RatingItem(a, ratingsMap.getOrDefault(a, 0L)))
                .collect(toList());

        return new RatingsSummary(ratings);
    }
}
