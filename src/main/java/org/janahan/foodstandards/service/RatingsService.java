package org.janahan.foodstandards.service;

import org.janahan.foodstandards.domain.RatingsSummary;
import reactor.core.publisher.Mono;

public interface RatingsService {
    /**
     * Asynchronously return a summary of the ratings of all establishments in an authority area
     * The ratings will be summed up and group based on the ratings available for the region.
     */
    Mono<RatingsSummary> ratingsSummaryForAuthorityAsync(long authorityId);
}
