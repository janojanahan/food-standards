package org.janahan.foodstandards.integration;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.jayway.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.janahan.foodstandards.FoodstandardsApplication;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FoodstandardsApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"app.external.fsa.baseUrl=http://localhost:8089"})
public class AuthoritiesIntegrationTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    @Value("${local.server.port}")
    int port;

    @Before
    public void setup() {
        RestAssured.port = port;
    }


    @Test
    public void shouldFetchAuthorities() throws IOException {
        String givenJsonResponse = StreamUtils.copyToString(
                this.getClass()
                        .getResourceAsStream("/integrationtestsdata/basic_authorities_integration.json"),
                Charset.defaultCharset());

        stubFor(get(urlEqualTo("/authorities/basic"))
                .withHeader("x-api-version", equalTo("2"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(givenJsonResponse)));

        when()
                .get("/api/authority")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .body("name", hasSize(14))
                .body("idCode", contains(197, 198, 277, 158, 48, 334, 199, 132, 133, 200, 134, 278, 77, 249));
    }
}
