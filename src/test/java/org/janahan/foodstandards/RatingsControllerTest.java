package org.janahan.foodstandards;

import org.janahan.foodstandards.domain.RatingsSummary;
import org.janahan.foodstandards.domain.RatingsSummary.RatingItem;
import org.janahan.foodstandards.service.RatingsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.janahan.foodstandards.domain.HygeneRatingLevel.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MockServletContext.class)
public class RatingsControllerTest {

    private final RatingsService ratingsService = mock(RatingsService.class);
    private final MockMvc mockMvc =
            MockMvcBuilders.standaloneSetup(new RatingsController(ratingsService))
                    .build();

    @Test
    public void shouldReturnRatingsSummaryAsJson() throws Exception {
        long authId = 56L;

        List<RatingItem> ratings = Arrays.asList(
                new RatingItem(THREE, 10L),
                new RatingItem(TWO, 0L),
                new RatingItem(ONE, 3L),
                new RatingItem(PASS, 7L),
                new RatingItem(EXEMPT, 5L)
        );
        RatingsSummary ratingsSummary = new RatingsSummary(ratings);

        when(ratingsService.ratingsSummaryForAuthorityAsync(authId))
                .thenReturn(Mono.just(ratingsSummary));

        MvcResult mvcResult = mockMvc.perform(get("/api/authority/{id}/ratings", authId))
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(instanceOf(RatingsSummary.class)))
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ratings.*", hasSize(5)))
                .andExpect(jsonPath("$.ratings[0].ratingLevel").value("3 Stars"))
                .andExpect(jsonPath("$.ratings[0].establishmentCount").value(10))
                .andExpect(jsonPath("$.ratings[1].ratingLevel").value("2 Stars"))
                .andExpect(jsonPath("$.ratings[1].establishmentCount").value(0))
                .andExpect(jsonPath("$.ratings[2].ratingLevel").value("1 Stars"))
                .andExpect(jsonPath("$.ratings[2].establishmentCount").value(3))
                .andExpect(jsonPath("$.ratings[3].ratingLevel").value("Pass"))
                .andExpect(jsonPath("$.ratings[3].establishmentCount").value(7))
                .andExpect(jsonPath("$.ratings[4].ratingLevel").value("Exempt"))
                .andExpect(jsonPath("$.ratings[4].establishmentCount").value(5))
                .andExpect(jsonPath("$.totalEstablishments").value(25))
                .andReturn();
    }
}