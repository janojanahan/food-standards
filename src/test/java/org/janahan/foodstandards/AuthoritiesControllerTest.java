package org.janahan.foodstandards;

import org.janahan.foodstandards.domain.Authority;
import org.janahan.foodstandards.service.AuthoritiesRepository;
import org.janahan.foodstandards.service.FsaServerException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpServerErrorException;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MockServletContext.class)
public class AuthoritiesControllerTest {

    private final AuthoritiesRepository authoritiesRepository = mock(AuthoritiesRepository.class);

    private final MockMvc mockMvc =
            MockMvcBuilders.standaloneSetup(new AuthoritiesController(authoritiesRepository))
                    .build();


    @Test
    public void shouldReturnAuthoritiesListAsJson() throws Exception {
        List<Authority> listOfAuthorities = Arrays.asList(
                new Authority(1L, "Whistling Weaver", null),
                new Authority(2L, "Shortlands", null)
        );

        when(authoritiesRepository.findAllAuthoritiesAsync())
                .thenReturn(Mono.just(listOfAuthorities));

        MvcResult mvcResult = mockMvc.perform(get("/api/authority"))
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(listOfAuthorities))
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.*", hasSize(2)))
                .andExpect(jsonPath("$.[0].idCode").value(1))
                .andExpect(jsonPath("$.[0].name").value("Whistling Weaver"))
                .andExpect(jsonPath("$.[1].idCode").value(2))
                .andExpect(jsonPath("$.[1].name").value("Shortlands"))
                .andReturn();
    }

    @Test
    public void shouldHandleFsaServiceErrorWithServiceUnavailable() throws Exception {

        when(authoritiesRepository.findAllAuthoritiesAsync())
                .thenReturn(Mono.error(new FsaServerException("fsa not available",
                        new HttpServerErrorException(HttpStatus.BAD_GATEWAY))));

        MvcResult mvcResult = mockMvc.perform(get("/api/authority"))
                .andExpect(request().asyncStarted())
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isServiceUnavailable())
                .andExpect(content().string("fsa not available"))
                .andReturn();
    }

    @Test
    public void shouldHandleAllOtherThrowablesWithServerError() throws Exception {

        when(authoritiesRepository.findAllAuthoritiesAsync())
                .thenReturn(Mono.error(new IllegalStateException("boom")));

        MvcResult mvcResult = mockMvc.perform(get("/api/authority"))
                .andExpect(request().asyncStarted())
                .andReturn();

        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string("Unknown Error: boom"))
                .andReturn();
    }

}